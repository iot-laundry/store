FROM node:10-alpine

EXPOSE 80

ADD . /app
WORKDIR /app

RUN npm install

CMD node server-nr.js

