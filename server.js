const { server } = require('./lib/restifyHelper.js');
const { log } = require('./lib/logHelper.js');

const { indexRoute } = require('./routes/index.js');
const { storeImgRoute } = require('./routes/storeImg.js');

indexRoute(server);
storeImgRoute(server);

module.exports = {
  server: server
}
