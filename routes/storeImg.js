const { storeHelper } = require('../lib/storeHelper.js');
const { log } = require('../lib/logHelper.js');

function rspStore(req, res, next) {
  
  // Get timestamp
  var ts = new Date().getTime();
  var predict = req.params.predict;
  var filename = 'rpi-' + ts + '-' + predict + '.jpg';

  // Store the picture
  if (predict > 0) {
    var fullFilename = storeHelper.storePicture(ts, filename, req.body, predict);
    log.info(fullFilename);
  }

  // Return a success result
  var rspInfo = {
    filename: fullFilename,
    size: req.body.length
  }
  res.send(rspInfo);
  next();
}

module.exports = {
  storeImgRoute: function(server) {
    server.post('/storeImg/:predict', rspStore);
  }
}