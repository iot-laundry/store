const sinon = require('sinon');
const request = require('supertest');
const fs = require('fs');

const { server } = require('../server.js');
const { storeHelper } = require('../lib/storeHelper');

var stubStorePicture = null;

describe('Server test', function() {
  before(function(done) {
    // Mock out the storeHelper library
    stubStorePicture = sinon.stub(storeHelper, "storePicture").resolves('filename.jpg')
    done();
  })

  it('Handles GET / and responds with JSON', function(done) {
    request(server)
      .get('/')
      .expect('Content-Type', /json/)
      .expect(200, done);
  })

  it('Handles POST /storeImg/:predict:', function(done) {
    let testimg = fs.readFileSync(__dirname + '/testimg.jpg');

    request(server)
      .post('/storeImg/1')
      .send(testimg)
      .set('Content-Type', 'image/jpeg')
      .expect('Content-Type', /json/)
      .expect(200, done);
  })

  after(function(done){
    stubStorePicture.restore();
    server.close(done);
  })
})